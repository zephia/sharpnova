<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\SsOpp
 *
 * @property int $id
 * @property int|null $owner_id
 * @property int|null $lead_id
 * @property int|null $deal_stage_id
 * @property int|null $amount
 * @property int|null $is_won
 * @property int|null $is_closed
 * @property string|null $close_date
 * @property string|null $close_notes
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOpp newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOpp newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOpp query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOpp whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOpp whereCloseDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOpp whereCloseNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOpp whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOpp whereDealStageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOpp whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOpp whereIsClosed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOpp whereIsWon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOpp whereLeadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOpp whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOpp whereUpdatedAt($value)
 */
	class SsOpp extends \Eloquent {}
}

namespace App{
/**
 * App\SsOwner
 *
 * @property int $id
 * @property string|null $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOwner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOwner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOwner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOwner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsOwner whereName($value)
 */
	class SsOwner extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\SsLead
 *
 * @property int $id
 * @property string|null $owner
 * @property string|null $last_name
 * @property string|null $first_name
 * @property string $email
 * @property string|null $primary_campaign
 * @property string|null $phone_number
 * @property string|null $city
 * @property string|null $state
 * @property string|null $vehicle_license
 * @property string|null $vehicle_brand
 * @property string|null $vehicle_model
 * @property int|null $vehicle_year
 * @property string|null $enquiry_type
 * @property string|null $want_call
 * @property string|null $city_other
 * @property string|null $owner_assign_mode
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereCityOther($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereEnquiryType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereOwner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereOwnerAssignMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead wherePrimaryCampaign($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereVehicleBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereVehicleLicense($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereVehicleModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereVehicleYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsLead whereWantCall($value)
 */
	class SsLead extends \Eloquent {}
}

namespace App{
/**
 * App\SsDealStage
 *
 * @property int $id
 * @property string|null $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsDealStage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsDealStage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsDealStage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsDealStage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SsDealStage whereName($value)
 */
	class SsDealStage extends \Eloquent {}
}

