<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/datastudio-sync', function () {
    \App\Jobs\DatastudioSharpspringSync::dispatch();
    return '';
});

Route::get('/enargas-sync', function () {
    \App\Jobs\EnargasSharpspringHistorySync::dispatch();
    return '-';
});

Route::get('/', function () {
    return '';
});

/*new leads*/
Route::get('/new-lead/ta/{ownerSlug}', 'TAManualLeadFormController@form');
Route::post('/new-lead/ta/{ownerSlug}', 'TAManualLeadFormController@formProcess');

Route::get('/new-lead/{ownerSlug}', 'ManualLeadFormController@form');
Route::post('/new-lead/{ownerSlug}', 'ManualLeadFormController@formProcess');
/**/
