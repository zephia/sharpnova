const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.setPublicPath(path.normalize('public'));
/*Front*/
mix.scripts(['resources/plugins/jquery.js',
        'resources/plugins/bootstrap-3.3.7/js/modal.js',
        'resources/plugins/bootstrap-3.3.7/js/tooltip.js',
        'resources/plugins/bootstrap-3.3.7/js/dropdown.js',
        'resources/plugins/bootstrap-3.3.7/js/carousel.js',
        'resources/plugins/bootstrap-3.3.7/js/transition.js',
        'resources/plugins/moment.js',
        'resources/plugins/bootstrap-daterangepicker/daterangepicker.js',
        'resources/plugins/select2/js/select2.full.js',
        'resources/plugins/jquery-ui/jquery-ui.js',
        'resources/plugins/fontawesome-iconpicker/js/fontawesome-iconpicker.js',
        'resources/plugins/Chart.js',
        'resources/plugins/Chart.PieceLabel.min.js',
        'resources/panel/js/addtohomescreen.js',
        'resources/panel/js/Layout.js',
        'resources/panel/js/PushMenu.js',
        'resources/panel/js/Tree.js',
        'resources/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js',
        'resources/plugins/typeahead/typeahead.bundle.js',
        'resources/plugins/bootstrap-fileinput/js/fileinput.js',
        'resources/plugins/bootstrap-fileinput/js/locales/es.js',
        'resources/plugins/bootstrap-fileinput/themes/fas/theme.js',
        'resources/plugins/bootstrap-fileinput/themes/explorer-fas/theme.js',
        'resources/panel/js/main.js'], 'public/js/build-panel.js')
    .copyDirectory('resources/panel/images-out', 'public/images')
    .less('resources/panel/less/main.less', 'public/css/build-panel.css')
    .options({
        //processCssUrls: false
    });
if (mix.inProduction()) {
    mix.version();
}