<?php

namespace App\Enargas;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;
use Exception;


class NovagncEnargasApiClient
{
    const API_PREFIX = 'Enargas';
    private $guzzleClient;
    private $api_username;
    private $api_password;

    /**
     * @param $base_uri
     * @param $api_username
     * @param $api_password
     * @throws Exception
     */
    public function __construct($base_uri, $api_username, $api_password)
    {
        if (empty($api_username) || empty($api_password)) {
            throw new Exception('Username and password are required.');
        }

        if (empty($base_uri)) {
            throw new Exception('Base URI is required.');
        }

        $this->api_username = $api_username;
        $this->api_password = $api_password;

        $this->guzzleClient = new GuzzleClient([
            'base_uri' => $base_uri,
            'verify' => false,
        ]);
    }

    /**
     * @param $method
     * @param $endpoint
     * @param array $params
     * @return mixed
     * @throws Exception
     */
    private function request($method, $endpoint, $params = [])
    {
        if (empty($method)) {
            throw new Exception('Method is required.');
        }

        if (empty($endpoint)) {
            throw new Exception('Endpoint is required.');
        }

        $params['pUser'] = $this->api_username;
        $params['pPass'] = $this->api_password;

        try {
            $response = $this->guzzleClient
                ->request($method, $endpoint, [
                    'json' => $params
                ]);

            return json_decode($response->getBody());
        } catch (GuzzleException $e) {
            // TODO: improve error handling
            dump($e->getMessage());

            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();

            Log::error('API Error', [
                $responseBodyAsString
            ]);

            throw new Exception('API Request ERROR: ' . $responseBodyAsString);
        }
    }

    public function getDominio($dominio)
    {
        try {
            $response = $this->request(
                'POST',
                $this::API_PREFIX . '/GetDominio',
                [
                    'pDominio' => $dominio
                ]
            );

            return $response;
        } catch (Exception $e) {
            dump('getProducts Error', $e);
            Log::error('getProducts Error', [
                $e->getMessage()
            ]);
        }
    }
}
