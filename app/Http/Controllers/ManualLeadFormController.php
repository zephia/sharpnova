<?php

namespace App\Http\Controllers;

use App\SsOwner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Redirect;
use SharpSpring\RestApi\Connection;
use SharpSpring\RestApi\CurlClient;
use SharpSpring\RestApi\SharpSpringRestApiException;

class ManualLeadFormController extends Controller
{
    private $ss_api_client;
    private $ss_api;
    private $users = [
        'eurokraft' => [
            313504273 => 'Adrian Alexis Peralta'
        ],
        'novagnc' => [
            313516434 => 'Christian Allende',
            313507650 => 'Jonathan Cabrera',
            313504274 => 'Martín Doniquian',
            313507648 => 'Rodrigo Mariani',
            313504271 => 'Roque Daniel Aguirre'
        ]
    ];

    private $sale_channel = [
        'eurokraft' => 'EuroKraft',
        'novagnc' => 'NovaGNC'
    ];

    public function __construct()
    {
        $this->ss_api_client = new CurlClient([
            'account_id' => env('SS_NOVAGNC_ACCOUNT_ID'),
            'secret_key' => env('SS_NOVAGNC_SECRET_KEY')
        ]);
        $this->ss_api = new Connection($this->ss_api_client);
    }

    public function form(Request $request, $ownerSlug)
    {
        $api_campaigns = $this->ss_api->call('getCampaigns', [
            'where' => []
        ]);

        if(empty($this->users[$ownerSlug])) {
            abort(404);
        }

        $campaigns = [];

        foreach ($api_campaigns['campaign'] as $api_campaign) {
            $campaigns[$api_campaign['id']] = $api_campaign['campaignName'];
        }

        unset($campaigns[817198083]);

        asort($campaigns);

        return view('new-lead.form', [
            'ownerSlug' => $ownerSlug,
            'campaigns' => $campaigns,
            'users' => $this->users[$ownerSlug],
        ]);
    }

    public function formProcess(Request $request, $ownerSlug)
    {
        $lead = $request->except('_token');

        if (!empty($request->get('ownerID'))) {
            $lead['ownerID'] = $request->get('ownerID');
        } else {
            unset($lead['ownerID']);
        }

        $lead['canal_de_ventas_5eb080ee5a110'] = $this->sale_channel[$ownerSlug];
        //$lead['re_ingresar_a_flujo_de_venta_5efcdef384830'] = 'Si';
        $lead['carga_manual_5ec2da5706a5c'] = 'Si';

        try {
            $leadResult = $this->ss_api->createLead($lead);
        } catch (SharpSpringRestApiException $e) {
            Log::info(static::class . ': Cannot create lead', [
                'message'=> $e->getMessage(),
                'lead' => $lead
            ]);
            if ($e->getCode() == 301) {
                $existingLeadByEmail = $this->ss_api->getLeads([
                    'emailAddress' => $lead['emailAddress']
                ]);

                $existingLeadByEmail[0] = array_merge($existingLeadByEmail[0], $lead);

                try {
                    $leadResult = $this->ss_api->updateLead($existingLeadByEmail[0]);
                    Log::warning(static::class . ': Update Lead Error', [
                        'message'=> $e->getMessage(),
                        'leadResult' => $leadResult
                    ]);
                } catch (SharpSpringRestApiException $e2) {
                    dd($e2->getMessage());
                }
            }
        }

        return Redirect::action('ManualLeadFormController@form', [
            'ownerSlug' => $ownerSlug,
            'success' => true
        ]);
    }
}
