<?php

namespace App\Http\Controllers;

use App\SsOwner;
use Illuminate\Http\Request;
use Redirect;
use SharpSpring\RestApi\Connection;
use SharpSpring\RestApi\CurlClient;
use SharpSpring\RestApi\SharpSpringRestApiException;

class TAManualLeadFormController extends Controller
{
    public function form(Request $request, $ownerSlug)
    {
        return view('new-lead.ta-form', [
            'ownerSlug' => $ownerSlug,
        ]);
    }

    public function formProcess(Request $request, $ownerSlug)
    {
        $ss_api_client = new CurlClient([
            'account_id' => env('SS_' . strtoupper($ownerSlug) . '_ACCOUNT_ID'),
            'secret_key' => env('SS_' . strtoupper($ownerSlug) . '_SECRET_KEY')
        ]);

        $ss_api = new Connection($ss_api_client);

        $lead = $request->except('_token');

        try {
            $leadResult = $ss_api->createLead($lead);
        } catch (SharpSpringRestApiException $e) {
            if ($e->getCode() == 301) {
                $existingLeadByEmail = $ss_api->getLeads([
                    'emailAddress' => $lead['emailAddress']
                ]);

                $existingLeadByEmail[0] = array_merge($existingLeadByEmail[0], $lead);

                try {
                    $leadResult = $ss_api->updateLead($existingLeadByEmail[0]);
                } catch (SharpSpringRestApiException $e2) {
                    dd($e2->getMessage());
                }
            } else {
                dd($e->getMessage());
            }
        }

        return Redirect::action('TAManualLeadFormController@form', [
            'ownerSlug' => $ownerSlug,
            'success' => true
        ]);
    }
}
