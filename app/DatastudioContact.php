<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatastudioContact extends Model
{
    protected $guarded = [];

    public $timestamps = false;
}
