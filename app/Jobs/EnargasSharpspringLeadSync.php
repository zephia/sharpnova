<?php

namespace App\Jobs;

use App\Enargas\NovagncEnargasApiClient;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use SharpSpring\RestApi\Connection;
use SharpSpring\RestApi\CurlClient;
use SharpSpring\RestApi\SharpSpringRestApiException;

class EnargasSharpspringLeadSync implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $lead;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($lead)
    {
        $this->lead = $lead;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $lead_email = $this->lead['emailAddress'];

        // Prevent e-mail update to avoid errors
        unset($this->lead['emailAddress']);

        Log::debug('Enargas SharpSpring lead sync Job dispatched for ' . $lead_email .
            ' - ' . $this->lead['patente_5ea20ee81032d']);

        // Connect to Enargas
        $enargas_api_client = new NovagncEnargasApiClient(
            env('NOVAGNC_ENARGAS_API_URI'),
            env('NOVAGNC_ENARGAS_API_USERNAME'),
            env('NOVAGNC_ENARGAS_API_PASSWORD')
        );

        // Connect to SharpSpring
        $ss_api_client = new CurlClient([
            'account_id' => env('SS_NOVAGNC_ACCOUNT_ID'),
            'secret_key' => env('SS_NOVAGNC_SECRET_KEY')
        ]);
        $ss_api = new Connection($ss_api_client);

        $response = $enargas_api_client->getDominio($this->lead['patente_5ea20ee81032d']);

        if (empty($response->error)) {
            $regulador1 = array_pop($response->data->datosRegulador);
            $cilindro1 = array_pop($response->data->datosCilindro);
            $cilindro2 = array_pop($response->data->datosCilindro);

            $ss_pec_data = [
                "eg_fecha_de_operacion_627a9e6f5c5cf" => $response->data->fechaOperacion,
                "eg_tipo_de_operacion_627a9eaf80ffc" => $response->data->datosOperacion->operacion,
                "eg_pec_codigo_627a9efd853e4" => $response->data->datosPEC->codigo,
                "eg_pec_nombre_627aa0f966f5a" => $response->data->datosPEC->razonSocial,
                "eg_codigo_taller_627a9f0cb8915" => $response->data->datosTaller->codigo,
                "eg_regulador_fabricante_627a9f1c4ed8c" => $regulador1->fabricante ?? '',
                "eg_regulador_627a9f27c01dc" => ($regulador1->codigoHomologacion ?? '') . ' - ' .
                    ($regulador1->numeroSerie ?? ''),
                "eg_cilindro_fabricante_627a9f3623cc8" => $cilindro1->fabricante ?? '',
                "eg_cilindro_1_627a9f4c57ae7" => ($cilindro1->codigoHomologacion ?? '') . ' - ' .
                    ($cilindro1->numeroSerie ?? ''),
                "eg_cilindro_2_627a9f56705e0" => ($cilindro2->codigoHomologacion ?? '') . ' - ' .
                    ($cilindro2->numeroSerie ?? ''),
                "eg_fecha_de_actualizacion_627a9d8be221d" => date('Y-d-m H:i'),
            ];

            $this->lead = array_merge($this->lead, $ss_pec_data);

            try {
                $ss_api->updateLead($this->lead);
                Log::info('Enargas SharpSpring lead sync UPDATED for ' .
                    $lead_email . ' - ' . $this->lead['patente_5ea20ee81032d']);
            } catch (SharpSpringRestApiException $e2) {
                Log::error('Enargas SharpSpring lead sync ERROR UPDATING LEAD for ' .
                    $lead_email . ' on SharpSpring!', [
                    'message' => $e2->getMessage()
                ]);
                $this->fail();
            }
        } elseif ($response->error == '02') {
            Log::info('Enargas SharpSpring lead sync NOT FOUND for ' .
                $lead_email . ' - ' . $this->lead['patente_5ea20ee81032d']);

            $ss_pec_data = [
                "eg_fecha_de_actualizacion_627a9d8be221d" => date('Y-d-m H:i'),
            ];

            $this->lead = array_merge($this->lead, $ss_pec_data);

            try {
                $ss_api->updateLead($this->lead);
            } catch (SharpSpringRestApiException $e2) {
                Log::error('Enargas SharpSpring lead sync ERROR UPDATING LEAD for ' .
                    $lead_email . ' on SharpSpring!', [
                    'message' => $e2->getMessage()
                ]);
                $this->fail();
            }
        } else {
            Log::error('Enargas SharpSpring lead sync error for ' .
                $lead_email . ' - ' . $this->lead['patente_5ea20ee81032d'] . '!', [
                    'response' => $response
                ]
            );
            $this->fail();
        }
        Log::debug('Enargas SharpSpring lead sync Job completed! for ' . $lead_email .
            ' - ' . $this->lead['patente_5ea20ee81032d']);
    }
}
