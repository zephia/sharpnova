<?php

namespace App\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use SharpSpring\RestApi\Connection;
use SharpSpring\RestApi\CurlClient;
use SharpSpring\RestApi\SharpSpringRestApiException;

class EnargasSharpspringHistorySync implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $lead;

    public $timeout = 36000;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('Enargas SharpSpring history Job dispatched!');

        // Connect to SharpSpring
        $ss_api_client = new CurlClient([
            'account_id' => env('SS_NOVAGNC_ACCOUNT_ID'),
            'secret_key' => env('SS_NOVAGNC_SECRET_KEY')
        ]);
        $ss_api = new Connection($ss_api_client);

        try {
            Log::info('Enargas SharpSpring history: Fetching leads from SharpSpring API...');

            $offset = 0;
            $energias_api_leads_all = [];
            do {
                Log::info('Enargas SharpSpring history: fetching offset ' . $offset . ' ...');
                $energias_api_call = $ss_api->call('getLeadsDateRange', [
                    'startDate' => '2010-01-01 00:00:00',
                    'endDate' => now()->format('Y-m-d H:i:s'),
                    'timestamp' => 'create',
                    'limit' => 5000,
                    'offset' => $offset,
                    'fields' => [
                        "emailAddress",
                        "patente_5ea20ee81032d",
                        "eg_fecha_de_actualizacion_627a9d8be221d",
                    ],
                ]);
                $energias_api_leads = $energias_api_call['lead'];
                Log::info('Enargas SharpSpring history: fetched ' . count($energias_api_leads) . ' leads');
                unset($energias_api_call);
                foreach ($energias_api_leads as $lead) {
                    $energias_api_leads_all[] = $lead;
                }
                $offset += 5000;
            } while (count($energias_api_leads) > 0);

            unset($energias_api_leads);

            Log::info('Enargas SharpSpring history: Total ' . count($energias_api_leads_all)
                . ' leads fetched form SharpSpring API');

            $sanitized_leads = [];

            Log::info('Enargas SharpSpring history: Sanitizing leads...');

            foreach ($energias_api_leads_all as $api_lead) {
                try {
                    if (
                        !empty($api_lead['patente_5ea20ee81032d']) &&
                        (
                            ($this->checkDaysOld($api_lead['eg_fecha_de_actualizacion_627a9d8be221d']) ?? 0) > 30 ||
                            empty($api_lead['eg_fecha_de_actualizacion_627a9d8be221d'])
                        )
                    ) {
                        $sanitized_licence = $this->sanitizeLicence($api_lead['patente_5ea20ee81032d']);
                        if ($this->isValidLicence($sanitized_licence)) {
                            $api_lead['new_lead'] = empty($api_lead['eg_fecha_de_actualizacion_627a9d8be221d']);
                            $api_lead['patente_5ea20ee81032d'] = $sanitized_licence;
                            $sanitized_leads[] = $api_lead;
                        }
                    }
                } catch(\Exception $e) {
                    Log::error('Enargas SharpSpring history: ERROR Sanitizing lead', [
                        'message' => $e->getMessage(),
                        'lead' => $api_lead
                    ]);
                }
            }

            unset($energias_api_leads_all);

            $lead_count = count($sanitized_leads);

            Log::info('Enargas SharpSpring history: ' . $lead_count . ' sanitized leads');
            Log::info('Enargas SharpSpring history: Estimated finish time: ' . now()->addSeconds($lead_count));

            foreach ($sanitized_leads as $lead_index => $sanitized_lead) {
                $delay_time = now()->addSeconds($lead_index);

                if ($sanitized_lead['new_lead']) {
                    EnargasSharpspringLeadSync::dispatch($sanitized_lead)
                        ->delay(now()->addDays(6));
                }

                unset($sanitized_lead['new_lead']);
                EnargasSharpspringLeadSync::dispatch($sanitized_lead)
                    ->delay($delay_time);

                Log::debug('Enargas SharpSpring history: ' . ($lead_index + 1) . ' - Job queued at ' . $delay_time .
                    ' for ' . $sanitized_lead['emailAddress'] . ' - ' . $sanitized_lead['patente_5ea20ee81032d']);
            }
        } catch (SharpSpringRestApiException $e2) {
            Log::error('Enargas SharpSpring history Job error!', [$e2->getMessage()]);
        }

        Log::info('Enargas SharpSpring history Job completed!');
    }

    private function sanitizeLicence($licence)
    {
        $license = str_replace(
            [
                ' ',
                '-',
                '.',
                ',',
            ],
            [
                '',
                '',
                '',
                '',
            ],
            $licence
        );
        $license = strtoupper(trim($license));

        return $license;
    }

    private function isValidLicence($licence)
    {
        return preg_match('/^([a-zA-Z]{2,3})([0-9]{3})([a-zA-Z]{2})?$/', $licence);
    }

    private function checkDaysOld($date)
    {
        return Carbon::parse($date)->diffInDays(now()) ?? 0;
    }
}
