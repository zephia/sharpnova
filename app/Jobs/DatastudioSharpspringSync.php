<?php

namespace App\Jobs;

use App\DatastudioContact;
use App\DatastudioOpp;
use App\DatastudioSyncLog;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use SharpSpring\RestApi\Connection;
use SharpSpring\RestApi\CurlClient;
use SharpSpring\RestApi\SharpSpringRestApiException;

class DatastudioSharpspringSync implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 3600;

    private $api_limit_per_call = 1000;

    private $contactSyncFields = [
        "id",
        "accountID",
        "ownerID",
        //"companyName",
        "campaignName",
        "ownerName",
        "title",
        "title",
        "title",
        "firstName",
        "lastName",
        "street",
        "city",
        "country",
        "state",
        "zipcode",
        "emailAddress",
        //"website",
        "phoneNumber",
        "officePhoneNumber",
        "phoneNumberExtension",
        "mobilePhoneNumber",
        //"faxNumber",
        "description",
        "campaignID",
        "trackingID",
        //"industry",
        "active",
        "isQualified",
        "isContact",
        "isCustomer",
        "status",
        "updateTimestamp",
        "createTimestamp",
        "leadScoreWeighted",
        "leadScore",
        "isUnsubscribed",
        "leadStatus",
        "persona",
        "marca_5e8f5fbd97331",
        "modelo_5e8f5fde83602",
        "patente_5ea20ee81032d",
        "tipo_de_consulta_5ea20f1a2c54d",
        "deseo_que_me_llamen_5ea20f7b9bc92",
        "a__o_de_fabricaci__n_5ea20fb96fda0",
        "fb_plataforma_5ea20fef4420b",
        "fb_nombre_de_campa__a_5ea2100351018",
        "fb_nombre_de_grupo_de_anuncios_5ea2101239327",
        "fb_nombre_de_anuncio_5ea2102524a4f",
        "utm_source_5ea210351f967",
        "utm_campaing_5ea210477cd0f",
        "utm_medium_5ea21054640dd",
        "utm_term_5ea210611fc1d",
        "utm_content_5ea2107009f30",
        "filomena_form_5ea72287b0f65",
        "filomena_lead_id_5ea72287b5ae5",
        "forma_pago_5ea73ba32db98",
        "kms_5ea73ba3346ac",
        "motor_5ea73ba33b861",
        "fb_ad_id_5eaca0faa086b",
        "canal_de_ventas_5eb080ee5a110",
        "completo_form_nutrici__n_vehiculo_5eb19331ef1b0",
        //"vendedores_nova_5ebac74e5bf74",
        //"vendedores_nova__1__5ebac77d6376e",
        "canal_de_ingreso_5ebb10820ad2e",
        "carga_manual_5ec2da5706a5c",
        "energias_opp_id_5ecee1465c6a7",
        //"fd_usuario_5ed53387f33c4",
        //"fd_tipo_usuario_5ed533948a4e4",
        //"fd_supervisor_5ed5339f0e5eb",
        //"fd_cargado_por_5ed533afe7fd9",
        //"fd_estado_de_gesti__n_5ed533bfa001a",
        //"fd_fecha_de_inicio_de_gesti__n_5ed533c87d40b",
        //"fd_fecha_de_primera_gesti__n_5ed533d1e850a",
        //"fd_fecha_de_ultima_gesti__n_5ed533dc6b135",
        //"fd_sexo_5ed533ee8df8a",
        //"nro__documento_5ed5342c917ff",
        //"fd_fecha_modificacion_5ed53436d8637",
        "fecha_nacimiento_5ed5344b556f8",
        //"fd_web_formulario_5ed534680806a",
        //"fd_equipo_que_le_interesa_al_cliente_5ta_generacio_5ed534fa17f17",
        //"fd_equipo_que_le_interesa_al_cliente_convencional_5ed5350c74771",
        //"fd_equipo_que_le_interesa_al_cliente_inyeccion_dir_5ed5352f997ea",
        //"fd___c__mo_nos_conoci___el_cliente__5ed53548c4adf",
        //"razon_social_5ed5356052202",
        "fd_taller_instalacion_5ed535a0ddb47",
        "fd_desde_que_medio_se_contacto_el_cliente__5ed535af7f04d",
        //"fd_justifique_el_motivo_de_rechazo_5ed53628206cd",
        //"fd_motivo_del_rechazo___desinteresado_5ed53652c3580",
        //"fd_motivo_del_rechazo___otro_5ed5366901c3c",
        //"fd_motivo_del_rechazo___finalizaci__n_de_gestiones_5ed536805d2f9",
        //"fd_motivo_del_rechazo___no_atiende_5ed5368cb5942",
        //"fd_gesti__n_motivo_del_rechazo___precio_5ed553670cdc7",
        //"fd_gesti__n_motivo_del_rechazo___sin_l__mite_en_la_5ed5537f4436e",
        //"fd_gesti__n_motivo_del_rechazo___instal___en_otro__5ed55391ced65",
        //"fd_gesti__n_motivo_del_rechazo___c__sar_5ed553adc733d",
        //"fd_gesti__n_motivo_del_rechazo___inyecci__n_direct_5ed553bdb7570",
        //"fd_gesti__n_motivo_del_rechazo___problema_del_ases_5ed553ca46092",
        //"gesti__n_motivo_del_rechazo___diesel_5ed553db915ff",
        //"fd_gesti__n_motivo_del_rechazo___financiaci__n_5ed553ef5a259",
        //"fd_gesti__n_motivo_del_rechazo___no_quiero_perder__5ed553fc81fb3",
        //"fd_gesti__n_motivo_del_rechazo___disponibilidad_de_5ed5540e8e296",
        "gesti__n_motivo_del_rechazo___pendientes_sin_atend_5ed55439912e3",
        //"fd_gesti__n_motivo_del_rechazo___seguimiento_semi__5ed5544d7af0b",
        //"fd_campa__as_para_definir_una_operaci__n_5ed55462ceed2",
        //"fd_n___factura_5ed55600831e7",
        "re_ingresar_a_flujo_de_venta_5efcdef384830",
        "recibi___voucher_promocional_5ffc88e1c410f",
        "cantidad_de_veh__culos_a_nombre_del_contacto_60df58fef1879",
        "eg_fecha_de_operacion_627a9e6f5c5cf",
        "eg_tipo_de_operacion_627a9eaf80ffc",
        "eg_pec_codigo_627a9efd853e4",
        "eg_pec_nombre_627aa0f966f5a",
        "eg_codigo_taller_627a9f0cb8915",
        "eg_regulador_fabricante_627a9f1c4ed8c",
        "eg_regulador_627a9f27c01dc",
        "eg_cilindro_fabricante_627a9f3623cc8",
        "eg_cilindro_1_627a9f4c57ae7",
        "eg_cilindro_2_627a9f56705e0",
        "eg_fecha_de_actualizacion_627a9d8be221d",
    ];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug(static::class . ' Job dispatched!');

        /*
         * Sync Logs
         */
        $current_date_secured = now()->subMinute()->format('Y-m-d H:i:s');

        $last_sync = DatastudioSyncLog::where('success', true)
            ->orderBy('created_at', 'desc')
            ->first();

        if (!$last_sync) {
            $last_sync = DatastudioSyncLog::create([
                'created_at' => $current_date_secured,
                'success' => 1
            ]);
        }

        $sync_log = DatastudioSyncLog::create([
            'created_at' => $current_date_secured,
        ]);

        $sync_from_date = $last_sync->created_at->format('Y-m-d H:i:s');

        Log::debug(static::class . ': Job sync dates', [$sync_from_date, $current_date_secured]);

        $ss_api_client = new CurlClient([
            'account_id' => env('SS_NOVAGNC_ACCOUNT_ID'),
            'secret_key' => env('SS_NOVAGNC_SECRET_KEY')
        ]);
        $ss_api = new Connection($ss_api_client);

        $campaigns = $this->getCampaigns($ss_api);
        $owners = $this->getOwners($ss_api);
        $oppDealStages = $this->getOppDealStages($ss_api);

        Log::debug(static::class . ': Sync Contacts');
        $remoteContacts = $this->getAllContacts($ss_api, $sync_from_date, $current_date_secured);
        foreach ($remoteContacts as $index => $remoteContact) {
            $remoteContact = $this->injectField(
                $remoteContact,
                'campaignName',
                $campaigns->where('id', $remoteContact['campaignID'])->first()->campaignName ?? ''
            );

            $remoteContact = $this->injectField(
                $remoteContact,
                'ownerName',
                $owners->where('id', $remoteContact['ownerID'])->first()->displayName ?? ''
            );

            if ($index == 0) {
                $this->upsertContactFields($remoteContact);
            }

            $contact = DatastudioContact::find($remoteContact['id']);

            if (!$contact) {
                $contact = new DatastudioContact;
            }

            $this->syncContact($remoteContact, $contact);
        }

        Log::debug(static::class . ': Sync Opps');
        $remoteOpps = $this->getAllOpps($ss_api, $sync_from_date, $current_date_secured);
        foreach ($remoteOpps as $index => $remoteOpp) {
            $remoteOpp = $this->injectField(
                $remoteOpp,
                'campaignName',
                $campaigns->where('id', $remoteOpp['campaignID'])->first()->campaignName ?? ''
            );

            $remoteOpp = $this->injectField(
                $remoteOpp,
                'ownerName',
                $owners->where('id', $remoteOpp['ownerID'])->first()->displayName ?? ''
            );

            $remoteOpp = $this->injectField(
                $remoteOpp,
                'dealStageName',
                $oppDealStages->where('id', $remoteOpp['dealStageID'])->first()->dealStageName ?? ''
            );

            if ($index == 0) {
                $this->upsertOppFields($remoteOpp);
            }

            $remoteOpp['id'] = (int)$remoteOpp['id'];

            $opp = DatastudioOpp::find($remoteOpp['id']);

            //dd($remoteOpp, $opp);

            if (!$opp) {
                $opp = new DatastudioOpp;
            }

            $this->syncOpp($remoteOpp, $opp);
        }

        //dump($remoteContacts, $remoteOpps);

        $sync_log->updated_at = now();
        $sync_log->success = true;
        $sync_log->save();

        Log::debug(static::class . ': Job completed!');
    }

    /**
     * @throws \Exception
     */
    private function getAllContacts($ss_api, $start_date, $end_date)
    {
        Log::debug(static::class . ': Get Contacts from SharpSpring API...');

        $offset = 0;
        $api_contacts_all = [];
        try {
            do {
                Log::debug(static::class . ': fetching offset ' . $offset . ' ...');
                $api_call = $ss_api->call('getLeadsDateRange', [
                    'startDate' => $start_date,
                    'endDate' => $end_date,
                    'timestamp' => 'update',
                    'limit' => $this->api_limit_per_call,
                    'offset' => $offset,
                    'fields' => $this->contactSyncFields,
                ]);
                $api_leads = $api_call['lead'];
                Log::debug(static::class . ': fetched ' . count($api_leads) . ' leads');
                unset($api_call);
                foreach ($api_leads as $contact) {
                    $api_contacts_all[] = $contact;
                }
                $offset += $this->api_limit_per_call;
            } while (count($api_leads) > 0);
            unset($api_leads);
        } catch (SharpSpringRestApiException $e2) {
            Log::error(static::class . ': getAllContacts error!', ['error' => $e2->getMessage()]);
            throw new \Exception($e2->getMessage());
        }

        Log::debug(static::class . ': Total ' . count($api_contacts_all)
            . ' leads fetched form SharpSpring API');

        return $api_contacts_all;
    }

    private function upsertContactFields($remoteContact)
    {
        $table_name = (new DatastudioContact)->getTable();
        foreach ($this->contactSyncFields as $field) {
            if (!Schema::hasColumn($table_name, $field)) {
                Schema::Table($table_name, function ($table) use ($field) {
                    switch ($field) {
                        case 'id':
                        case 'ownerID':
                        case 'accountID':
                        case 'campaignID':
                        case 'trackingID':
                            $table->bigInteger($field);
                            break;
                        case 'active':
                        case 'isUnsubscribed':
                        case 'isQualified':
                        case 'isContact':
                        case 'isCustomer':
                            $table->addColumn('tinyInteger', $field, ['length' => 1])->nullable();
                            break;
                        case 'status':
                        case 'leadScore':
                        case 'leadScoreWeighted':
                            $table->tinyInteger($field)->nullable();
                            break;
                        case 'createTimestamp':
                        case 'updateTimestamp':
                            $table->dateTime($field);
                            break;
                        case 'firstName':
                        case 'lastName':
                        case 'zipcode':
                        case 'phoneNumber':
                        case 'officePhoneNumber':
                        case 'phoneNumberExtension':
                        case 'mobilePhoneNumber':
                            $table->string($field, 50)->nullable();
                            break;
                        case 'city':
                        case 'emailAddress':
                            $table->string($field, 100)->nullable();
                            break;
                        case 'description':
                            $table->text($field)->nullable();
                            break;
                        default:
                            $table->string($field, 255)->nullable();
                    }
                });
            }
        }
    }

    private function syncContact($remoteContact, $localContact)
    {
        foreach ($this->contactSyncFields as $field) {
            $localContact->{$field} = $remoteContact[$field];
        }
        $localContact->save();
    }

    /**
     * @throws \Exception
     */
    private function getAllOpps($ss_api, $start_date, $end_date)
    {
        Log::debug(static::class . ': Get Opps from SharpSpring API...');

        $offset = 0;
        $api_opps_all = [];
        try {
            do {
                Log::debug(static::class . ': fetching offset ' . $offset . ' ...');
                $api_call = $ss_api->call('getOpportunitiesDateRange', [
                    'startDate' => $start_date,
                    'endDate' => $end_date,
                    'timestamp' => 'update',
                    'limit' => $this->api_limit_per_call,
                    'offset' => $offset,
                ]);
                $api_opps = $api_call['opportunity'];
                Log::debug(static::class . ': fetched ' . count($api_opps) . ' opps');
                unset($api_call);
                foreach ($api_opps as $opp) {
                    $api_opps_all[] = $opp;
                }
                $offset += $this->api_limit_per_call;
            } while (count($api_opps) > 0);
            unset($api_opps);
        } catch (SharpSpringRestApiException $e2) {
            Log::error(static::class . ': getAllOpps error!', ['error' => $e2->getMessage()]);
            throw new \Exception($e2->getMessage());
        }

        Log::debug(static::class . ': Total ' . count($api_opps_all)
            . ' Opps fetched form SharpSpring API');

        return $api_opps_all;
    }

    private function upsertOppFields($remoteContact)
    {
        $table_name = (new DatastudioOpp)->getTable();
        foreach (array_keys($remoteContact) as $field) {
            if (!Schema::hasColumn($table_name, $field)) {
                Schema::Table($table_name, function ($table) use ($field) {
                    switch ($field) {
                        case 'id':
                        case 'ownerID':
                        case 'dealStageID':
                        case 'accountID':
                        case 'campaignID':
                        case 'primaryLeadID':
                        case 'originatingLeadID':
                            $table->bigInteger($field);
                            break;
                        case 'isCLosed':
                        case 'isWon':
                            $table->addColumn('tinyInteger', $field, ['length' => 1])->nullable();
                            break;
                        case 'createTimestamp':
                        case 'closeDate':
                            $table->dateTime($field);
                            break;
                        default:
                            $table->string($field, 255);
                    }
                });
            }
        }
    }

    private function syncOpp($remoteOpp, $localOpp)
    {
        foreach (array_keys($remoteOpp) as $field) {
            $localOpp->{$field} = $remoteOpp[$field];
        }

        try {
            $localOpp->save();
        } catch (\Exception $e) {
            Log::error(static::class . ': syncOpp error!', ['error' => $e->getMessage()]);
        }

    }

    private function getCampaigns($ss_api)
    {
        Log::debug(static::class . ': Sync Campaigns');
        $response = $ss_api->call('getCampaigns', [
            'where' => []
        ]);

        return collect($response['campaign'])->map(function ($item) {
            return (object)$item;
        });
    }

    private function getOwners($ss_api)
    {
        Log::debug(static::class . ': Sync owners');
        $response = $ss_api->call('getUserProfiles', [
            'where' => []
        ]);

        return collect($response['userProfile'])->map(function ($item) {
            return (object)$item;
        });
    }

    private function getOppDealStages($ss_api)
    {
        Log::debug(static::class . ': Sync Opp Deal Stages');
        $response = $ss_api->call('getDealStages', [
            'where' => []
        ]);

        return collect($response['dealStage'])->map(function ($item) {
            return (object)$item;
        });
    }

    private function injectField($data, $field, $value)
    {
        $data[$field] = $value;
        return $data;
    }
}
