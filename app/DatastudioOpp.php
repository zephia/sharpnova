<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatastudioOpp extends Model
{
    protected $guarded = [];

    public $timestamps = false;
}
