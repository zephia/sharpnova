@extends('new-lead.layouts.master')
@section('content')
    @if(request()->get('success'))
        <div class="feed">Lead creado con exito!</div>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".feed").fadeIn("fast", function () {
                    setTimeout(function () {
                        $(".feed").fadeOut("fast");
                    }, 3000);
                });
            });
        </script>
    @endif
    <div class="feed">Lead creado con exito!</div>
    <div class="container">
        <div class="text-center">
            <img src="/assets/img/logo-{{ $ownerSlug }}.png" alt="">
        </div>
        <form action="{{ action('ManualLeadFormController@formProcess', ['ownerSlug' => $ownerSlug]) }}" class="zlform"
              method="post" id="contact-form">
            @csrf
            <h1>Carga manual de prospecto</h1>
            <h3>Datos personales</h3>
            <div class="row">
                <div class="col-sm-6">
                    <input type="text" name="firstName" class="form-control" required id="first-name" placeholder="Nombre *">
                </div>
                <div class="col-sm-6">
                    <input type="text" name="lastName" class="form-control" id="last-name" placeholder="Apellido">
                </div>
                <div class="col-sm-6">
                    <input type="email" name="emailAddress" class="form-control" required id="email" placeholder="Email *">
                </div>
                <div class="col-sm-6">
                    <input type="text" name="phoneNumber" class="form-control" id="phone" placeholder="Teléfono">
                </div>
            </div>
            <h3>Datos del vehículo</h3>
            <div class="row">
                <div class="col-sm-4">
                    <input type="text" name="marca_5e8f5fbd97331" class="form-control" id="vehicle-brand" placeholder="Marca">
                </div>
                <div class="col-sm-4">
                    <input type="text" name="modelo_5e8f5fde83602" class="form-control" id="vehicle-model" placeholder="Modelo">
                </div>
                <div class="col-sm-4">
                    <input type="text" name="a__o_de_fabricaci__n_5ea20fb96fda0" class="form-control" id="vehicle-year" placeholder="Año de fabricación">
                </div>
                <div class="col-sm-6">
                    <input type="text" name="motor_5ea73ba33b861" class="form-control"
                           id="vehicle-displacement" placeholder="Cilindrada Motor">
                </div>
                <div class="col-sm-6">
                    <input type="text" name="patente_5ea20ee81032d" class="form-control" id="vehicle-domain" placeholder="Patente">
                </div>
                <div class="col-sm-12">
                    <label for="description">Descripción</label>
                    <textarea name="description" cols="40" rows="5" class="form-control" id="description"></textarea>
                </div>
            </div>
            <h3>Gestión</h3>
            <div class="row">
                <div class="col-sm-4">
                    <select name="ownerID" class="form-control" id="vehicle-brand">
                        <option value="">- Asignar a vendedor -</option>
                        @foreach($users as $id => $user)
                            <option value="{{ $id }}">{{ $user }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-4">
                    <select name="canal_de_ingreso_5ebb10820ad2e" class="form-control" id="income-chanel">
                        <option value="">- Canal de ingreso -</option>
                        <option value="Mostrador">Mostrador</option>
                        <option value="Telefonico">Telefonico</option>
                        <option value="Whatsapp">Whatsapp</option>
                        <option value="MyBusiness">MyBusiness</option>
                        <option value="MercadoLibre">MercadoLibre</option>
                        <option value="Face msn">Face msn</option>
                        <option value="Ig msn">Ig msn</option>
                    </select>
                </div>
                <div class="col-sm-4">
                    <label for="tipo-consulta"></label>
                    <select name="tipo_de_consulta_5ea20f1a2c54d" class="form-control" id="tipo-consulta">
                        <option value="">- Tipo de consulta -</option>
                        <option value="Ventas GNC">Ventas GNC</option>
                        <option value="Ventas Cubiertas">Ventas Cubiertas</option>
                        <option value="Venta Repuestos">Venta Repuestos</option>
                        <option value="Obleas">Obleas</option>
                        <option value="PostVenta">PostVenta</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label for="income-chanel">Campaña</label>
                    <select name="campaignID" class="form-control" id="campaign" required>
                        <option value="" ></option>
                        @foreach($campaigns as $campaign_id => $campaign_name)
                            <option value="{{ $campaign_id }}">{{ $campaign_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <button class="btn btn-primary btn-lg btn-block" id="btnSubmit" type="submit">CARGAR PROSPECTO</button>
        </form>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#contact-form").on('submit', function (e) {
                $("#btnSubmit").attr("disabled", true);
            });
        });
    </script>
@endsection
