<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css"/>
<script src="/assets/libs/jquery/jquery-2.1.1.min.js"></script>
<title>Carga manual de nuevo prospecto</title>
<style media="screen">
    h1 {
        text-transform: uppercase;
        font-size: 22px;
        text-align: center;
        font-weight: bold;
    }
    h3 {
        font-size: 18px;
        margin-bottom:20px;
        padding-bottom: 10px;
        border-bottom: 1px solid #CCC;
    }
    .form-control{
        margin-bottom: 10px;
    }
    img{
        margin: 30px 0 15px;
    }
    .feed {
        position: absolute;
        width: 300px;
        top: 30px;
        right: 30px;
        background-color: darkgreen;
        color: white;
        font-size: 18px;
        text-align: center;
        padding: 15px;
        border-radius: 5px;
        display: none;
    }
    form{
        margin-bottom: 40px;
    }
</style>
<body>
<main>
    @yield('content')
</main>
</body>
</html>
