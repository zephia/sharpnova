@extends('new-lead.layouts.master')
@section('content')
    @if(request()->get('success'))
        <div class="feed">Prospecto creado con exito!</div>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".feed").fadeIn("fast", function () {
                    setTimeout(function () {
                        $(".feed").fadeOut("fast");
                    }, 3000);
                });
            });
        </script>
    @endif
    <style>
        .feed {
            position: absolute;
            width: 300px;
            top: 30px;
            right: 30px;
            background-color: darkgreen;
            color: white;
            font-size: 18px;
            text-align: center;
            padding: 15px;
            border-radius: 5px;
            display: none;
        }
    </style>
    <div class="feed">Prospecto creado con exito!</div>
    <div class="container">
        <div class="text-center">
            <img src="/assets/img/logo-{{ $ownerSlug }}.png" alt="">
        </div>
        <form action="{{ action('TAManualLeadFormController@formProcess', ['ownerSlug' => $ownerSlug]) }}" class="zlform"
              method="post" id="contact-form">
            @csrf
            <h1>Carga manual de prospecto</h1>
            <h3>Datos personales</h3>
            <div class="row">
                <div class="col-sm-6">
                    <label for="first-name">Nombre *</label>
                    <input type="text" name="firstName" class="form-control" required id="first-name">
                </div>
                <div class="col-sm-6">
                    <label for="last-name">Apellido</label>
                    <input type="text" name="lastName" class="form-control" id="last-name">
                </div>
                <div class="col-sm-6">
                    <label for="email">Email *</label>
                    <input type="email" name="emailAddress" class="form-control" required id="email">
                </div>
                <div class="col-sm-6">
                    <label for="phone">Teléfono</label>
                    <input type="text" name="phoneNumber" class="form-control" id="phone">
                </div>
            </div>
            <h3>Datos del vehículo</h3>
            <div class="row">
                <div class="col-sm-4">
                    <label for="vehicle-brand">Marca</label>
                    <input type="text" name="marca_5e8f5fbd97331" class="form-control" id="vehicle-brand">
                </div>
                <div class="col-sm-4">
                    <label for="vehicle-model">Modelo</label>
                    <input type="text" name="modelo_5e8f5fde83602" class="form-control" id="vehicle-model">
                </div>
                <div class="col-sm-4">
                    <label for="vehicle-year">Año de fabricación</label>
                    <input type="text" name="a__o_de_fabricaci__n_5ea20fb96fda0" class="form-control" id="vehicle-year">
                </div>
                <div class="col-sm-6">
                    <label for="vehicle-displacement">Cilindrada Motor</label>
                    <input type="text" name="motor_5ea73ba33b861" class="form-control"
                           id="vehicle-displacement">
                </div>
                <div class="col-sm-6">
                    <label for="vehicle-domain">Patente</label>
                    <input type="text" name="patente_5ea20ee81032d" class="form-control" id="vehicle-domain">
                </div>
                <div class="col-sm-12">
                    <label for="description">Descripción</label>
                    <textarea name="description" cols="40" rows="5" class="form-control" id="description"></textarea>
                </div>
            </div>
            <button class="btn btn-primary btn-lg btn-block" id="btnSubmit" type="submit">CARGAR PROSPECTO</button>
        </form>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#contact-form").on('submit', function (e) {
                $("#btnSubmit").attr("disabled", true);
            });
        });
    </script>
@endsection
